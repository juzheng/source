# 在你的网站上养一只3D小马！

![](https://ws4.sinaimg.cn/large/006tKfTcgy1fph16uk4xgj31490pn7ni.jpg)

关注EqCN的朋友们可能已经注意到了，我们在网站的右下角增添了一只3D云宝黛茜。RD的眼睛好奇地注视着你的鼠标指针，简直萌呆了是不是！

我使用了Live2dWidget.js与Frash的小马模型来制作这个挂件，参见：

Live2dWidget.js：[https://l2dwidget.js.org](https://l2dwidget.js.org)

Pony Model：[https://steamcommunity.com/sharedfiles/filedetails/?id=578539741&searchtext=](https://steamcommunity.com/sharedfiles/filedetails/?id=578539741&searchtext=)

如果你的网站和EqCN一样使用wordpress的话，可以直接安装我编写的插件（在GPLv2下开源）：

WP 3D Pony：[https://github.com/juzeon/wp-3d-pony](https://github.com/juzeon/wp-3d-pony)

OK，那么我先来演示一下在wordpress平台上的安装。首先下载插件：

	cd /path/to/wordpress/wp-content/plugins/ #cd到你的wordpress插件目录
	git clone https://github.com/juzeon/wp-3d-pony wp-3d-pony #用git工具clone源码
	#如果是linux注意chown、chmod改一下权限
	
或者下载这个包，在wp后台上传安装激活：[https://gitee.com/juzheng/source/blob/master/wp-3d-pony.zip](https://gitee.com/juzheng/source/blob/master/wp-3d-pony.zip)

安装完之后进入后台的设置——WP 3D Pony：

![](https://ws1.sinaimg.cn/large/006tKfTcgy1fpgzdi0wb5j30kr0c0ta1.jpg)

下面是一些设置项目：

> Activation: 	激活或者关闭
>
> Texture Path: 	材质路径，需要使用相对路径。前面不能加`/`。默认是RD
> 
> Position: 	显示位置，右还是左
> 
> Width: 	宽度
> 
> Height: 	高度。高度和宽度建议不要改，默认的高宽是最适合小马模型的了
> 
> Horizontal Offset: 	水平偏移，就是从页面边缘往里面移多少
> 
> Vertical Offset: 	垂直偏移，就是从页面下面往上移多少
> 
> Common Scale: 	电脑版的模型缩放，调大模型会变大，调小模型会变小。可以是小数
> 
> Default Opacity: 	默认不透明度，调成1就是完全不透明
> 
> Opacity On Hover: 	鼠标放上去时候的不透明度
> 
> Mobile Display: 	要不要在手机上显示
> 
> Mobile Scale: 手机的模型缩放

材质存在`插件路径/live2dw/assets/pony/` 。材质图片看起来像这样：

![](https://ws4.sinaimg.cn/large/006tKfTcgy1fph0qdfrykj31kw1kwguv.jpg)

我当前绘制了一个RD一个小呆的。你可以尝试自己修改，PS打开，找张小马图片，把鬃毛抠出来往里面贴，眼睛瞳色和身体颜色改一下，出来就是你的OC了！

如果你的网站不用wordpress，那就只能手动配置了。你需要配置一个用以设置模型的json，然后在需要展示模型的页面增加相应的L2DWidget.init的代码。我写了一个demo可以参考一下：[https://gitee.com/juzheng/source/blob/master/3d-pony-html.zip](https://gitee.com/juzheng/source/blob/master/3d-pony-html.zip)

其中L2DWidget更详细的设置参数可以参考官方文档：[https://l2dwidget.js.org/docs/class/src/index.js~L2Dwidget.html](https://l2dwidget.js.org/docs/class/src/index.js~L2Dwidget.html)

OK，最后祝大家与自己的小马玩的开心！
